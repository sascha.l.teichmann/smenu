# smenu

A simple wrapper around `dmenu` which remembers your last
choices and displays them in front of the menu.

It is similiar to [<del>xboomx</del>](https://bitbucket.org/dehun/xboomx/wiki/Home) 
but it is written in Go and not another wrapper
around `dmenu_path` and `dmenu_run`. It has its own cache file and stores the names of the executable together with the
number of calls as plain text.


## Installation

Install **suckless-tools** to obtain `dmenu`. On a Debian alike systems something like

```shell
apt-get install suckless-tools
```

will do. To build `smenu` you need to have a **Go 1.x** (x >= 18) development environment available.
See [go.dev/dl](https://go.dev/dl/) how to get one. 

```shell
go install https://gitlab.com/sascha.l.teichmann/smenu
```

Place the resulting executable 'smenu' in your `PATH`.

Development setup:

```shell
git clone https://gitlab.com/sascha.l.teichmann/smenu
cd smenu
go build 
```

To use it e.g with [i3](https://i3wm.org) add following line to your `~/.i3/config`:

```
bindsym $mod+d exec smenu
```

To pass options to the call of dmenu can be just appended them
after a `--`. So `smenu -- -b -l 10` will result in a call of `dmenu -b -l 10`.
You can specify a custom executable with the `-d` flag.
This is e.g. needed if you have a HiDPI display and the standard
font rendering is too tiny. Using the [XFT patch](http://tools.suckless.org/dmenu/patches/xft) helps.
In this case your binding may look like:

```
bindsym $mod+d exec smenu -d '/home/user/bin/dmenu' -- -fn DejaVuSansMono-10
```

# How does it work?

`smenu` generates a list of executable files
in your `PATH`. They are stored in a cache file in `~/.smenu_cache`. Each entry
in this file has counter how often the executable is called from smenu.
The most frequently called executable are on the top of the list
an therefore lead the list processed by dmenu. After calling of dmenu
the position of the selected entry is updated in `~/.smenu_cache`
according to its increased counter.

## License

This Free Software is covered by the terms of the GPLv3+. See [LICENSE](./LICENSE) for details.  
&copy; 2013, 2014, 2023 by [Sascha L. Teichmann](mailto:teichmann@intevation.de)
